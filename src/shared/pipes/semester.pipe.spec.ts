import {TestBed, async, inject} from '@angular/core/testing';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { SemesterPipe } from './semester.pipe';
import {ConfigurationService} from '../services/configuration.service';
import {CommonModule} from '@angular/common';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MostModule} from '@themost/angular';
import {TestingConfigurationService} from '../../../testing/src/testing-configuration.service';

// tslint:disable quotemark
const translations_en = {
    "Semester": {
        "short": "{{value}}",
        "medium": "{{ordinal}}",
        "long": "{{ordinal}} Semester",
        "full": {
            "251": "Winter Semester",
            "252": "Summer Semester",
            "255": "Winter/Summer Semester"
        }
    }
};
const translations_el = {
    "Semester": {
        "short": "{{value}}",
        "medium": "{{value}}ο",
        "long": "{{value}}ο Εξάμηνο",
        "full": {
            "251": "Χειμερινό Εξάμηνο",
            "252": "Εαρινό Εξάμηνο",
            "255": "Χειμερινό/Εαρινό Εξάμηνο"
        }
    }
};
// tslint:enable quotemark

describe('SemesterPipe', () => {
    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            imports: [
                CommonModule,
                HttpClientTestingModule,
                TranslateModule.forRoot(),
                MostModule.forRoot({
                    base: '/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                })
            ],
            providers: [
                {
                    provide: ConfigurationService,
                    useClass: TestingConfigurationService
                }
            ]
        }).compileComponents().then( () => {
            const translateService = TestBed.get(TranslateService);
            // configure translations
            translateService.setTranslation('en', translations_en, true);
            translateService.setTranslation('el', translations_el, true);
        });
    }));
    it('should create an instance', inject([TranslateService],
        (translateService: TranslateService) => {
        const semesterPipe = new SemesterPipe(translateService);
        expect(semesterPipe).toBeTruthy();
    }));
    it('should display long format', inject([TranslateService],
        (translateService: TranslateService) => {
            translateService.currentLang = 'en';
            const semesterPipe = new SemesterPipe(translateService);
        console.log('INFO', 'TRANSLATE', semesterPipe.transform(1, 'long'));
        expect(semesterPipe.transform(1)).toBe('1st Semester');
    }));
    it('should pass object', inject([TranslateService],
        (translateService: TranslateService) => {
            const semesterPipe = new SemesterPipe(translateService);
            translateService.currentLang = 'en';
        const value = {
            id: 1
        };
        console.log('INFO', 'TRANSLATE', semesterPipe.transform(value, 'long'));
        expect(semesterPipe.transform(1)).toBe('1st Semester');
        expect(typeof value).toBe('object');
    }));
    it('should display medium format', inject([TranslateService],
        (translateService: TranslateService) => {
            const semesterPipe = new SemesterPipe(translateService);
            translateService.currentLang = 'en';
        console.log('INFO', 'TRANSLATE', semesterPipe.transform(1, 'medium'));
        expect(semesterPipe.transform(1, 'medium')).toBe('1st');
    }));
    it('should display short format', inject([TranslateService],
        (translateService: TranslateService) => {
            const semesterPipe = new SemesterPipe(translateService);
            translateService.currentLang = 'en';
        console.log('INFO', 'TRANSLATE', semesterPipe.transform(1, 'short'));
        expect(semesterPipe.transform(1, 'short')).toBe('1');
    }));
    it('should display long format in greek', inject([TranslateService],
        (translateService: TranslateService) => {
            const semesterPipe = new SemesterPipe(translateService);
            translateService.currentLang = 'el';
        console.log('INFO', 'TRANSLATE', semesterPipe.transform(1, 'long'));
        expect(semesterPipe.transform(1)).toBe('1ο Εξάμηνο');
    }));
    it('should display medium format in greek', inject([TranslateService],
        (translateService: TranslateService) => {
            const semesterPipe = new SemesterPipe(translateService);
            translateService.currentLang = 'el';
        console.log('INFO', 'TRANSLATE', semesterPipe.transform(1, 'medium'));
        expect(semesterPipe.transform(1, 'medium')).toBe('1ο');
    }));
    it('should display short format', inject([TranslateService],
        (translateService: TranslateService) => {
            const semesterPipe = new SemesterPipe(translateService);
            translateService.currentLang = 'el';
        console.log('INFO', 'TRANSLATE', semesterPipe.transform(1, 'short'));
        expect(semesterPipe.transform(1, 'short')).toBe('1');
    }));
});
