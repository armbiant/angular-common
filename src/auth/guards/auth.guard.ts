import {Inject, Injectable, InjectionToken} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import { UserService } from '../services/user.service';
import { Observable } from 'rxjs';
import { ErrorService } from '../../error/error.service';
import {ConfigurationService} from '../../shared/services/configuration.service';
import {LocationPermission} from './auth.guard.interfaces';

export let APP_LOCATIONS = new InjectionToken('app.locations');

export const DEFAULT_APP_LOCATIONS = [{
    privilege: 'Location',
    target: {
        url: '^/auth/'
    },
    mask: 1
  },
  {
    privilege: 'Location',
    target: {
        url: '^/error'
    },
    mask: 1
  }
];

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(private _router: Router,
    @Inject(APP_LOCATIONS) private _locations: Array<LocationPermission>,
    private _errorService: ErrorService,
    private _configurationService: ConfigurationService,
    private _userService: UserService) {

    // merge app locations, if any
    if (this._configurationService.settings && this._configurationService.settings.auth) {
      // get extra locations
      const appLocations = this._configurationService.settings.auth.locations;
      if (Array.isArray(appLocations)) {
        // get APP_LOCATIONS
        if (this._locations) {
          this._locations.unshift.apply(this._locations, appLocations);
        }
      }
    }
    this._locations.forEach((x) => {
      if (typeof x.target.pattern === 'undefined' || x.target.pattern === null) {
        x.target.pattern = new RegExp(x.target.url, 'i');
      }
    });
  }

  public canActivateLocation(path: string, user: any): LocationPermission {

    let groups = [];
    if (user && user.groups) {
      groups = user.groups.map((x) => {
        return x.name;
      });
    }
    return this._locations.find((x) => {
      return x.target.pattern.test(path)
        && (typeof x.account === 'undefined' || groups.indexOf(x.account.name) >= 0)
        && (x.mask === 0 || ((x.mask & 1) === 1))
        && user;
    });

  }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    return this._canActivate(state);
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this._canActivate(state);
  }

  private _canActivate(state: RouterStateSnapshot): Observable<boolean> | boolean {
    return new Observable<boolean>(resolve => {
      this._userService.getUser().then((res) => {
        const location = this.canActivateLocation(state.url, res);
        if (location && (typeof location.account === 'undefined')) {
          return resolve.next(true);
        }
        if (res) {
          if (location && (location.mask & 1) === 1) {
            return resolve.next(true);
          } else {
            if (location && location.redirectTo) {
              this._router.navigate([location.redirectTo]);
              return resolve.next(false);
            }
            // noinspection JSIgnoredPromiseFromCall
            this._router.navigate(['/error/403.1'], {
              queryParams: {
                action: 'Error.LoginAsDiffrentUser',
                continue: '/auth/loginAs'
              }
            });
            return resolve.next(false);
          }
        }
        // noinspection JSIgnoredPromiseFromCall
        this._router.navigate(['/auth/login'], { queryParams: {
            continue: state.url
          }});
        return resolve.next(false);
      });
    });
  }
}
