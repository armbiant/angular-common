import {ChangeDetectorRef, Injectable, Optional, Inject, InjectionToken} from '@angular/core';
import {TranslateService, TranslatePipe} from '@ngx-translate/core';

export declare interface AppSidebarNavigationItem {
  key: string;
  url: string;
  name?: string;
  class?: string;
  index?: number;
  icon?: string;
  children?: Array<AppSidebarNavigationItem>;
}

export let SIDEBAR_LOCATIONS = new InjectionToken<Array<AppSidebarNavigationItem>>('app.sidebar.locations');

class TranslationChangeDetector extends ChangeDetectorRef {
  checkNoChanges(): void {
  }

  detach(): void {
  }

  detectChanges(): void {
  }

  markForCheck(): void {
  }

  reattach(): void {
  }

}

@Injectable({
  providedIn: 'root'
})
export class AppSidebarService {

  public navigationItems: Array<AppSidebarNavigationItem> = [];
  private readonly _changeDetector: TranslationChangeDetector;
  constructor(private _translateService: TranslateService,
    @Inject(SIDEBAR_LOCATIONS) private sidebarLocations: Array<AppSidebarNavigationItem>) {
    this._changeDetector = new TranslationChangeDetector();
  }

  public loadConfig(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.addRange(this.sidebarLocations);
      return resolve();
    });
  }

  /**
   * Adds one or more navigation items to application sidebar
   * @param {AppSidebarNavigationItem} item
   */
  public add(...item: Array<AppSidebarNavigationItem>): void {
        // initialize translate pipe with null change detector
        const pipe = new TranslatePipe(this._translateService, this._changeDetector);
        // add navigation items to array
        this.navigationItems.push.apply(this.navigationItems, item.map(navigationItem => {
          // translate name
        const x = Object.assign(navigationItem, {
          name: pipe.transform(navigationItem.key)
        });
        if (Array.isArray(x.children)) {
          // translate children
          x.children = x.children.map( child => {
            return Object.assign(child, {
              name: pipe.transform(child.key)
            });
          });
        }
        return x;
      }));
  }

  /**
   * Adds a collection of navigation items to application sidebar
   * @param {Array<AppSidebarNavigationItem>} items
   */
  public addRange(items: Array<AppSidebarNavigationItem>): void {
    return this.add.apply(this, items);
  }

  /**
   * Remove one navigation item from application sidebar
   * @param {AppSidebarNavigationItem} item
   */
  public remove(item: AppSidebarNavigationItem): void {
    // find item to remove
    const itemIndex = this.navigationItems.findIndex(el => el.name === item.name || el.url === item.url ||  el.key === item.key);
    // if item is found
    if (itemIndex !== -1) {
      this.navigationItems.splice(itemIndex, 1, );
    }
  }

  /**
   * Removes one or more navigation items from application sidebar
   * @param {Array<AppSidebarNavigationItem>} items
   */
  public removeRange(items: Array<AppSidebarNavigationItem>): void {
    for (const item of items) {
      this.remove(item);
    }
  }

  /**
   * Adds one or more navigation items to as children to a sidebar item
   * @param {AppSidebarNavigationItem} parent
   * @param {AppSidebarNavigationItem} item
   */
  public addChild(parent: AppSidebarNavigationItem, ...item: Array<AppSidebarNavigationItem>): void {
    // initialize translate pipe with null change detector
    const pipe = new TranslatePipe(this._translateService, this._changeDetector);
    // translate child
    const translatedChild = item.map(navigationItem => {
      // translate name
      const x = Object.assign(navigationItem, {
        name: pipe.transform(navigationItem.key)
      });
      if (Array.isArray(x.children)) {
        // translate children
        x.children = x.children.map( child => {
          return Object.assign(child, {
            name: pipe.transform(child.key)
          });
        });
      }
      return x;
    });
    // find parent by index and insert into place
    const parentIndex = this.navigationItems.findIndex(el => el.name === parent.name || el.url === parent.url || el.key === parent.key);
    if (parentIndex) {
      const parentWithChildren = this.navigationItems[parentIndex];
      const childrenArray = parentWithChildren.children || [];
      childrenArray.push.apply(childrenArray, translatedChild);
      parentWithChildren.children = childrenArray;
      // Replaces parent with parentWithChildren
      this.navigationItems.splice(parentIndex, 1, parentWithChildren);
    }
  }

  /**
   * Adds a collection of navigation items to as children to a parent
   * @param {AppSidebarNavigationItem} parent
   * @param {Array<AppSidebarNavigationItem>} items
   */
  public addChildren(parent: AppSidebarNavigationItem, items: Array<AppSidebarNavigationItem>): void {
    return this.addChild.apply(this, [parent, ...items]);
  }
}
