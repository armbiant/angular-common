import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserService } from './user.service';

@Injectable({
     providedIn: 'root'
})
export class ActivatedUser {

    public user: BehaviorSubject<any> = new BehaviorSubject(this.activatedUser);

    constructor(private _userService: UserService) {
    }

    private get activatedUser() {
        return this._userService.getUserSync();
    }

}
