import {Injectable, ComponentFactoryResolver, ApplicationRef, Injector, EmbeddedViewRef} from '@angular/core';
import {ToastComponent} from '../components/modal/toast.component';


/**
 * @export
 * @class ToastService
 */
@Injectable()
export class ToastService {

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private appRef: ApplicationRef,
              private injector: Injector) { }

  /**
   * Shows a toast message
   * @param {string} title A string which represents message title
   * @param {string} message A string which represents message body
   * @param {boolean=} autoHide A boolean which indicates whether or not message will auto hide
   * @param {number=} delay A number which indicates the number of milliseconds before auto hide message
   */
  show(title: string, message: string, autoHide = true, delay = 5000) {
    // search for toast container
    let container = document.body.getElementsByClassName('universis-toast-container')[0];
    if (container == null) {
      // create toast container
      container = document.createElement('div');
      container.classList.add('universis-toast-container', 'p-3');
      // append to boyd
      document.body.appendChild(container);
    }
    // create a component reference for toast component
    const componentRef = this.componentFactoryResolver
        .resolveComponentFactory(ToastComponent)
        .create(this.injector);
    componentRef.instance.title = title;
    componentRef.instance.message = message;
    componentRef.instance.autoHide = autoHide;
    componentRef.instance.delay = delay;
    componentRef.location.nativeElement.classList.add('ml-auto');

    // attach component to the appRef so that it's inside the ng component tree
    this.appRef.attachView(componentRef.hostView);

    // get DOM element from component
    const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;

    // append DOM element to the body
    container.appendChild(domElem);

    container.classList.remove('hidden');

    setTimeout(() => {
      componentRef.instance.hide();
    }, delay);
  }

  // noinspection JSMethodCanBeStatic
  /**
   * Clears toast messages
   */
  clear() {
    // search for toast container
    const container = document.body.getElementsByClassName('universis-toast-container')[0];
    if (container) {
      // remove children
      while (container.firstChild) {
        container.removeChild(container.firstChild);
      }
    }
  }

}
